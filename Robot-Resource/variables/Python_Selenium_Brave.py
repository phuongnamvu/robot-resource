import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep

class PythonBlueway(unittest.TestCase):

    def setUp(self):
        
        driver_path = "C:/Program Files (x86)/Python/Scripts/chromedriver.exe"
        brave_path = "C:/Program Files (x86)/BraveSoftware/Brave-Browser/Application/brave.exe"
        
        option = webdriver.ChromeOptions()
        option.binary_location = brave_path
        
        #driver = webdriver.Chrome(executable_path=driver_path, chrome_options=option)
        
        self.driver = webdriver.Chrome(executable_path=driver_path, chrome_options=option)

    def test_login_blueway(self):
        driver = self.driver
        driver.get("http://cd.blueway.fr:20104/BWDesignerFaces/login.jsf")
        
        
        
        elem = driver.find_element_by_id("loginForm:username")
        elem.click()
        elem.send_keys("admin")
        
        elem = driver.find_element_by_id("loginForm:password")
        elem.click()
        elem.send_keys("admin")
        
        print("Login is valid ")
        
        #elem = driver.find_element_by_xpath("//button/span")
        #elem.click()
        #sleep(5)
        #print("Succesful Login Page ")
        #elem.send_keys(Keys.RETURN)
    
        #elem = driver.find_element_by_id("loginForm:j_idt19")
        elem = driver.find_element_by_xpath("//button/span")
        elem.click()
           
        print("Login successful ")
    #def tearDown(self):
     #   self.driver.close()
     

if __name__ == "__main__":
    unittest.main()