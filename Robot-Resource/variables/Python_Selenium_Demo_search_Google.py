import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        
    def test_search_in_google(self):
        driver = self.driver
        driver.get("https://www.google.com.vn/")
        
        
        #self.assertIn("Python", driver.title)
        elem = driver.find_element_by_name("q")
        elem.send_keys("Hello Google - My Name is Nam ")
        elem.send_keys(Keys.RETURN)
        # assert "No results found." not in driver.page_source
        sleep (5)

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()