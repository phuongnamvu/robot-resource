*** Settings ***
Documentation     A test suite with a single test for cd.blueway.fr:20104/BWDesignerFaces/login.jsf
...               Created by hats' Robotcorder
Library           Selenium2Library    timeout=10

*** Variables ***
${BROWSER}    chrome
${SLEEP}    3

*** Test Cases ***
cd.blueway.fr:20104/BWDesignerFaces/login.jsf test
    Open Browser    http://cd.blueway.fr:20104/BWDesignerFaces/login.jsf    ${BROWSER}
    
    Click Element    //input[@id="loginForm:username"]    
    Input Text    xpath=//input[@id="loginForm:username"]    admin
    Input Text    //input[@id="loginForm:password"]    admin
    

    Click Element    //span[@class="ui-button-text ui-c"]
    
    Maximize Browser Window
    
    #Set Browser Implicit Wait    3
    #Wait Until Page Contains Element    xpath=(//span[@class="ui-button-text ui-c"])[42]  
      
    Wait Until Element Is Visible        xpath=(//span[@class="ui-button-text ui-c"])[42]  
    Click Element    xpath=(//span[@class="ui-button-text ui-c"])[42]
    
    Wait Until Element Is Visible   xpath=(//span[@class="Menuitemname ui-draggable ui-draggable-handle"])[42]
    
    Click Element    xpath=(//span[@class="Menuitemname ui-draggable ui-draggable-handle"])[42]
     
       



    Wait Until Element Is Visible  xpath=(//LABEL[@title=''][text()='Nouveau'][text()='Nouveau'])[32] 
    Click Element                  xpath=(//LABEL[@title=''][text()='Nouveau'][text()='Nouveau'])[32] 
     
    #Click Element    xpath=(//span[@class="ui-button-text ui-c"])[42]
    
    #Click Element    xpath=(//label[@class="customDrag"])[53]
       

    Wait Until Element Is Visible      //input[@id="tabscontent:tabView:edittext_0_0"]
    Input Text                        //input[@id="tabscontent:tabView:edittext_0_0"]    Test_Record
    
    Wait Until Element Is Visible    //span[@class="ui-button-icon-left ui-icon ui-c fa fa-floppy-o faBigButtonIcon"]
    Click Element                    //span[@class="ui-button-icon-left ui-icon ui-c fa fa-floppy-o faBigButtonIcon"]

    #Close Browser