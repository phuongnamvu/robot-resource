*** setting *** 
Library    SeleniumLibrary 

  

*** Test Cases ***

LetGo
    AS2
    CDC
    File_trigger
    IDOC 
    Mail Trigger
    TCP

       
*** Keyword***      

   
 
    
    
AS2
    
    Open Browser                                  http://cd.blueway.fr:20104/BWDesignerFaces/login.jsf                       firefox
      
    Click Element                                 id=loginForm:username    
    Input Text                                    id=loginForm:username                                                      admin    
    Click Element                                 id=loginForm:password    
    Input Password                                id=loginForm:password                                                      admin   
    Click Element                                 xpath=//button/span    
    Maximize Browser Window
     
    Set Selenium Speed   0.5 
    Set Selenium Timeout   6
    
    Wait Until Page Contains Element              id=rigthmenu:idTopMenuItem:3:menuButton     
    Click Element                                 id=rigthmenu:idTopMenuItem:3:menuButton 
    
    Click Element                                 xpath=//SPAN[text()='Déclencheur AS2']       
        
    Click Element                                 xpath=(//LABEL[@title=''][text()='Nouveau'][text()='Nouveau'])[2]
    
    Wait Until Page Contains                      Déclencheur AS2 
      
    Wait Until Page Contains                      Nom de déclencheur         
       
                                     
    Input Text                                    xpath=//input[starts-with(@id,'tabscontent:tabView:trigger_edittext')]                                   AS2Trigger_robot  
    

   Wait Until Element Is Enabled                   css=.fa-floppy-o
   
    Click Element                                 css=.fa-floppy-o


CDC
    
    
    Set Selenium Speed   0.5 
    Set Selenium Timeout   6
    
    Wait Until Page Contains Element              id=rigthmenu:idTopMenuItem:3:menuButton     
    Click Element                                 id=rigthmenu:idTopMenuItem:3:menuButton 
    
    Click Element                                 xpath=//SPAN[text()='Déclencheur CDC']       
        
    Click Element                                 xpath=(//LABEL[@title=''][text()='Nouveau'][text()='Nouveau'])[3]
    
    Wait Until Page Contains                      Déclencheur CDC 
      
    Wait Until Page Contains                      Nom de déclencheur         
       
                                     
    Input Text                                    xpath=//input[starts-with(@id,'tabscontent:tabView:trigger_edittext')]                                   CDCTrigger_robot  
    
    Click Element                                  css=.fa-floppy-o 

File_trigger
    
    
    Set Selenium Speed   0.5 
    Set Selenium Timeout   6
    
    Wait Until Page Contains Element              id=rigthmenu:idTopMenuItem:3:menuButton     
    Click Element                                 id=rigthmenu:idTopMenuItem:3:menuButton  
    
    Click Element                                 xpath=//SPAN[text()='Déclencheur Fichier']       
        
    Click Element                                 xpath=(//LABEL[@title=''][text()='Nouveau'][text()='Nouveau'])[4]
    
    Wait Until Page Contains                      Déclencheur Fichier 
      
    Wait Until Page Contains                      Nom de déclencheur         
       
                                     
    Input Text                                    xpath=//input[starts-with(@id,'tabscontent:tabView:trigger_edittext')]                                   FileTrigger_robot  
    
   
    #Click Element                                 css=.fa-floppy-o
    
    
    Click Element                                   xpath=//SPAN[@class='ui-button-icon-left ui-icon ui-c fa fa-floppy-o faBigButtonIcon'] 

IDOC
    
    
    Set Selenium Speed   0.5 
    Set Selenium Timeout   6
    
    Wait Until Page Contains Element              id=rigthmenu:idTopMenuItem:3:menuButton     
    Click Element                                 id=rigthmenu:idTopMenuItem:3:menuButton    
    
    Click Element                                 xpath=//SPAN[text()='Déclencheur IDOC']       
        
    Click Element                                 xpath=(//LABEL[@title=''][text()='Nouveau'][text()='Nouveau'])[5]
    
    Wait Until Page Contains                      Déclencheur IDOC 
      
    Wait Until Page Contains                      Nom de déclencheur         
       
                                     
    Input Text                                    xpath=//input[starts-with(@id,'tabscontent:tabView:trigger_edittext')]                                   IDOCTrigger_robot  
    

   
   
    Click Element                                css=.fa-floppy-o
    

Mail Trigger
    
    
    Set Selenium Speed   0.5 
    Set Selenium Timeout   6
    
    Wait Until Page Contains Element              id=rigthmenu:idTopMenuItem:3:menuButton     
    Click Element                                 id=rigthmenu:idTopMenuItem:3:menuButton  
    
    Click Element                                 xpath=//SPAN[text()='Déclencheur Mail']       
        
    Click Element                                 xpath=(//LABEL[@title=''][text()='Nouveau'][text()='Nouveau'])[6]
    
    Wait Until Page Contains                      Déclencheur Mail 
      
    Wait Until Page Contains                      Nom de déclencheur         
       
                                     
    Input Text                                    xpath=//input[starts-with(@id,'tabscontent:tabView:trigger_edittext')]                                   Trigger_mail_robot  
    

   
   
    Click Element                                xpath=//SPAN[@class='ui-button-icon-left ui-icon ui-c fa fa-floppy-o faBigButtonIcon'] 
    

TCP
    
    
    Set Selenium Speed   0.5 
    Set Selenium Timeout   6
    
    Wait Until Page Contains Element              id=rigthmenu:idTopMenuItem:3:menuButton     
    Click Element                                 id=rigthmenu:idTopMenuItem:3:menuButton 
    
    Click Element                                 xpath=//SPAN[text()='Déclencheur TCP']       
        
    Click Element                                 xpath=(//LABEL[@title=''][text()='Nouveau'][text()='Nouveau'])[7]
    
    Wait Until Page Contains                      Déclencheur TCP 
      
    Wait Until Page Contains                      Nom de déclencheur         
       
                                     
    Input Text                                    xpath=//input[starts-with(@id,'tabscontent:tabView:trigger_edittext')]                                   Trigger_TCP_robot  
    


   
   
    Click Element                                 css=.fa-floppy-o
    