*** Settings ***

Library    SeleniumLibrary

#Variables       ../variables/global.py
# Resource        ../resources/browser.resource
# Resource        ../resources/log.resource
Test Setup      loginBlueway
# Test Teardown   logoutPortal

*** Test cases ***


TestPerf_Project    
    Import
    Check 
    Installation
    
    
   

*** Keyword***   

loginBlueway 
    Open Browser      http://cd.blueway.fr:20104/BWDesignerFaces/designer.jsf      Chrome 
        
 
    # Open Browser    http://cd.blueway.fr:20104/BWDesignerFaces/designer.jsf      Chrome 
    Click Element                                id=loginForm:username    
    Input Text                                   id=loginForm:username    admin    
    Click Element                                id=loginForm:password    
    Input Password                              id=loginForm:password     admin 
    Click Element                               xpath=//button/span    
    Maximize Browser Window    

Import 
    
    # 1 / Click on Setting
    Wait Until Element Is Visible       xpath=//a[@class='ui-commandlink ui-widget ad-menu-icon fa']   
    Click Element                        xpath=//a[@class='ui-commandlink ui-widget ad-menu-icon fa']   
    
    
    # 2/ Click : Import 
    Wait Until Element Is Visible        //span[@class='ui-menuitem-text' and text()='Import'] 
    Click Element                        //span[@class='ui-menuitem-text' and text()='Import'] 
      
    
    # 3/ Input : File Zip   
    
    Wait Until Element Is Visible        //input[@id='formImportation:test1']     
    Wait Until Page Contains    Import   
    Sleep    3        
    Input Text    xpath=//input[@id='formImportation:test1']     CheckPerf_200828_112018.zip
    Input Text    xpath=//input[@id='formImportation:test1']     CheckPerf_200828_112018.zip
    Input Text    xpath=//input[@id='formImportation:test1']     CheckPerf_200828_112018.zip
    


    # 4/ Click Load 
    Click Element   //span[@class='ui-button-text ui-c' and text()="Télécharger"] 
   
    
Check 
    Wait Until Page Contains    Import     
    Wait Until Page Contains   Importer un projet avec un nom
    Wait Until Page Contains    Prefix    

    # 5 / Input : Name 
    Wait Until Element Is Visible    xpath=//*[normalize-space(text()) and normalize-space(.)='Importer un projet avec un nom :'][1]/following::input[1]
    Click Element    xpath=//*[normalize-space(text()) and normalize-space(.)='Importer un projet avec un nom :'][1]/following::input[1]
    Click Element    xpath=//*[normalize-space(text()) and normalize-space(.)='Importer un projet avec un nom :'][1]/following::input[1]
      
    Input Text       xpath=//*[normalize-space(text()) and normalize-space(.)='Importer un projet avec un nom :'][1]/following::input[1]     Robot_Import    
    


    # 6/ Input : Prefix 
    

    Click Element        xpath=//*[normalize-space(text()) and normalize-space(.)='Prefix :'][1]/following::input[1]
    Input Text    xpath=//*[normalize-space(text()) and normalize-space(.)='Prefix :'][1]/following::input[1]           Robot_Test   
    

       

    # 7 / Import : Interface 
    Click Element      xpath=//SPAN[@class='ui-button-text ui-c'][text()='Import'][text()='Import'][1] 
    
    
     
           


  

    # 8/ Import : Support 
    Click Element     xpath=//SPAN[@class='ui-icon ui-icon-triangle-1-e'][1]
    #Click Element   xpath=//SPAN[@class='ui-icon ui-icon-triangle-1-e'][2] 
    #Click Element    xpath=//SPAN[@class='ui-button-text ui-c'and text()='Import']  
     Wait Until Element Is Visible   xpath=//div[@id='form:importlistData']/div[2]/div//div[@class='ui-datatable-scrollable-header-box']/table[@role='grid']//tr/th[3]//button[@role='button']/span[@class='ui-button-text ui-c']   
    Click Element    xpath=//div[@id='form:importlistData']/div[2]/div//div[@class='ui-datatable-scrollable-header-box']/table[@role='grid']//tr/th[3]//button[@role='button']/span[@class='ui-button-text ui-c']
       

                 
      
     # 9/ Click : Import 
     Click Element    xpath=//SPAN[@class='ui-button-text ui-c'][text()='Importer']
     

    #Reload Page
    
     
    
    # 10 / Wait : Notification Pop up 
    Set Selenium Speed    2   
    Set Selenium Timeout    15     

    Wait Until Page Contains        Notification
    Click Element    xpath=//SPAN[@class='ui-button-text ui-c'][text()='OK'][text()='OK'][1]
      


    

Installation
     
    # 11 / Double Click on : Project 
    Double Click Element    xpath=//label[@class='ui-outputlabel ui-widget projectName' and text()="Robot_Import"]     
    
    Wait Until Page Contains    Robot_Import    
    Click Element           xpath=//I[@class='fa fa-upload faLinkMenuIcon']   
    
    
    # 12 / Click on : Insatllation 
    
    Set Selenium Speed    3   
    Set Selenium Timeout    15  
    
    Wait Until Page Contains             Installer tout
    Click Element                        xpath=//div[@id='tabscontent:tabView:installAll_0']//span  
    


    # 13 / Click : Install  
    Click Element                          xpath=//SPAN[@class='ui-button-icon-left ui-icon ui-c fa fa-upload faBigButtonIcon']
    



    











    








    
    
    
    # Not Using : Open Window Pop Up  ( We will check Robot support Open Window Popup soon ) 
    # Wait Until Element Is Visible        //span[text()='Charger fichier'] 
    # Click Element                        //span[text()='Charger fichier'] 
    
     


