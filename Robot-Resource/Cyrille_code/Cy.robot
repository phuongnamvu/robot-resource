** Settings ***
Library         SeleniumLibrary
Library         OperatingSystem


*** Test cases ***
Open Google
     Open Google

Search Google
      Search Google



*** Variables ***
#01Welcome Page
${URLwelcome}           https://www.google.com
${BROWSER}              headlesschrome
${BROWSER1}              headlessfirefox
${SearchString}         blueway


*** Keywords ***
Open Google
      Open Browser            ${URLwelcome}    Chrome

Search Google
     input text           name:q                                ${SearchString}
     Click Element        xpath=(//input[@name="btnK"])[2]
     
   
     ${path} =            Capture Page Screenshot
     File Should Exist    ${path}
     





